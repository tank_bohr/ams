-ifndef(PACK_PB_H).
-define(PACK_PB_H, true).
-record(pack, {
    type = erlang:error({required, type}),
    user_id = erlang:error({required, user_id}),
    addressee_id,
    text
}).
-endif.

