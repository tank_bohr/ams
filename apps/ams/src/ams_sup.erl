-module(ams_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    Args = case application:get_env(master) of
        {ok, Master} ->
            [Master];
        undefined ->
            []
    end,
    {ok, { {one_for_one, 5, 10}, [
        {ams_replica, {ams_replica, start_link, Args}, permanent, 5000, worker, [ams_replica]}
    ]} }.

