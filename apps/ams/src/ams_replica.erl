-module(ams_replica).
-export([
    start_link/0,
    start_link/1,
    init_pack/2,
    message_pack/1
]).

-behaviour(gen_server).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-include("ams_pb.hrl").

-record (state, {
    sessions = ets:new(sessions, [named_table, set]),
    deferred_messages = ets:new(deferred_messages, [
        named_table,
        duplicate_bag,
        {keypos, #pack.addressee_id}
    ])
}).

-define(SERVER, ?MODULE).
-define (DEFFERED_MESSAGES_DELAY, 500).

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

start_link(Master) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [Master], []).

init_pack(UserId, Channel) ->
    gen_server:call(?SERVER, {init_pack, UserId, Channel}).

message_pack(Pack) ->
    gen_server:call(?SERVER, {message_pack, Pack}).

%% @private
init([]) ->
    {ok, #state{}};
init([Master]) ->
    State = #state{},
    case net_adm:ping(Master) of
        pong ->
            lager:notice("Sync with ~p", [Master]),
            {Sessions, DeferredMessages} = rpc:call(Master, gen_server, call, [?SERVER, get_all]),
            ok = lists:foreach(fun (Session) ->
                ok = store_session(Session, State)
            end, Sessions),
            true = ets:insert(State#state.deferred_messages, DeferredMessages);
        pang ->
            lager:warning("There is no any master")
    end,
    {ok, State}.

%% @private
handle_call({init_pack, UserId, Channel}, _From, State) ->
    Session = find_or_create_session(UserId, State),
    ok = ams_session:add_channel(Session, Channel),
    send_deferred_messages(Session, UserId, State),
    {reply, {ok, Session}, State};
handle_call({message_pack, Pack}, _From, State) ->
    send_or_defer_message(Pack, State),
    {reply, ok, State};
handle_call(get_all, _From, State = #state{sessions = Sessions, deferred_messages = DeferredMessages}) ->
    Result = {ets:tab2list(Sessions), ets:tab2list(DeferredMessages)},
    {reply, Result, State};
handle_call(_Request, _From, State) ->
    {reply, {error, unknown_call}, State}.

%% @private
handle_cast(_Msg, State) ->
    {noreply, State}.

%% @private
handle_info({store_session, SessionData}, State) ->
    ok = store_session(SessionData, State),
    {noreply, State};
handle_info({delete_session, Pid}, State = #state{sessions = Sessions}) ->
    true = ets:match_delete(Sessions, {'_', Pid, '_'}),
    {noreply, State};
handle_info({store_deferred_message, Pack}, State = #state{deferred_messages = DeferredMessages}) ->
    true = ets:insert(DeferredMessages, Pack),
    {noreply, State};
handle_info({delete_deferred_messages, Key}, State = #state{deferred_messages = DeferredMessages}) ->
    true = ets:delete(DeferredMessages, Key),
    {noreply, State};
handle_info({'DOWN', _Ref, process, Pid, Info}, State) ->
    lager:debug("Session ~p down: ~p", [Pid, Info]),
    rpc:abcast(?SERVER, {delete_session, Pid}),
    {noreply, State};
handle_info({nodedown, Node}, State = #state{sessions = Sessions}) ->
    lager:debug("Nodedown ~p. Cleanup sessions", [Node]),
    true = ets:match_delete(Sessions, {'_', '_', Node}),
    {noreply, State};
handle_info(Info, State) ->
    lager:warning("Unknown info: ~p", [Info]),
    {noreply, State}.

%% @private
terminate(_Reason, _State) ->
    ok.

%% @private
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

find_or_create_session(UserId, State) ->
    case find_session(UserId, State) of
        undefined -> start_session(UserId);
        Pid -> Pid
    end.

send_or_defer_message(Pack = #pack{addressee_id=AddresseeId}, State) ->
    case find_session(AddresseeId, State) of
        undefined ->
            rpc:abcast(?SERVER, {store_deferred_message, Pack});
        Session ->
            ams_session:send_message(Session, Pack)
    end.

send_deferred_messages(Session, UserId, #state{deferred_messages = DeferredMessages}) ->
    Messages = ets:lookup(DeferredMessages, UserId),
    lists:foreach(fun (Msg) ->
        ams_session:send_message(Session, Msg),
        timer:sleep(?DEFFERED_MESSAGES_DELAY)
    end, Messages),
    rpc:abcast(?SERVER, {delete_deferred_messages, UserId}).

find_session(UserId, #state{sessions = Sessions}) ->
    case ets:lookup(Sessions, UserId) of
        [{UserId, Pid, _Node}] -> Pid;
        [] -> undefined
    end. 

start_session(UserId) ->
    {ok, Pid} = ams_session:start_link(UserId),
    _Ref = erlang:monitor(process, Pid),
    rpc:abcast(?SERVER, {store_session, {UserId, Pid}}),
    Pid.

store_session({UserId, Pid}, State) ->
    Node = node(Pid),
    store_session({UserId, Pid, Node}, State);
store_session({UserId, Pid, Node}, #state{sessions = Sessions}) ->
    lager:debug("~p monitors node ~p", [?SERVER, Node]),
    true = erlang:monitor_node(Node, true),
    true = ets:insert(Sessions, {UserId, Pid, Node}),
    ok.
