-module(ams_protocol).
-behaviour(ranch_protocol).

-export([start_link/4]).
-export([init/4]).

-include("ams_pb.hrl").
 
start_link(Ref, Socket, Transport, Opts) ->
    Pid = spawn_link(?MODULE, init, [Ref, Socket, Transport, Opts]),
    {ok, Pid}.
 
init(Ref, Socket, Transport, _Opts = []) ->
    ok = ranch:accept_ack(Ref),
    loop(Socket, Transport).
 
loop(Socket, Transport) ->
    {OK, Closed, Error} = Transport:messages(),
    Transport:setopts(Socket, [{active, true}]),
    receive
        {OK, Socket, ProtobufMessage} ->
            %% lager:debug("Protobuf message ~p", [ProtobufMessage]),
            Pack = ams_pb:decode_pack(ProtobufMessage),
            ok = handle_pack(Pack, {self(), Transport, Socket}),
            loop(Socket, Transport);
        {Closed, Socket} ->
            lager:debug("Connection closed"),
            ok = Transport:close(Socket);
        {Error, Socket, Reason} ->
            lager:error("Socket error occured: ~p", Reason),
            ok = Transport:close(Socket);
        {'DOWN', _Ref, process, Pid, Info} ->
            lager:debug("Session ~p closed: ~p", [Pid, Info]),
            ok = Transport:close(Socket);
        {nodedown, Node} ->
            lager:warning("Nodedown ~p. Close connection", [Node]),
            ok = Transport:close(Socket);
        Unknown ->
            lager:warning("Unknown message: [~p]", [Unknown]),
            ok = Transport:close(Socket)
    end.

handle_pack(#pack{type='INIT', user_id=UserId}, Channel) ->
    lager:info("Init from user ~p", [UserId]),
    {ok, Session} = ams_replica:init_pack(UserId, Channel),
    _Ref = erlang:monitor(process, Session),
    true = erlang:monitor_node(node(Session), true),
    ok;
handle_pack(Pack = #pack{type='MESSAGE', user_id=UserId, addressee_id=AddresseeId, text=Text}, _Connection) ->
    lager:info("Message [~p] from ~p to ~p", [Text, UserId, AddresseeId]),
    ams_replica:message_pack(Pack).
