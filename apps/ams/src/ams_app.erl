-module(ams_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    {ok, _} = start_ranch(),
    ams_sup:start_link().

stop(_State) ->
    ok.

start_ranch() ->
    {ok, Port} = application:get_env(port),
    lager:debug("Port is ~p", [Port]),
    ranch:start_listener(ams, 100,
        ranch_tcp, [{port, Port}],
        ams_protocol, []
    ).
