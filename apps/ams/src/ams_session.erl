-module(ams_session).
-export([
    start_link/1,
    add_channel/2,
    send_message/2
]).

-behaviour(gen_server).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-record(state, {
    user_id,
    channels = []
}).

-include("ams_pb.hrl").


start_link(UserId) ->
    gen_server:start_link(?MODULE, [UserId], []).

add_channel(Pid, Channel) ->
    gen_server:cast(Pid, {add_channel, Channel}).

send_message(Pid, Message) ->
    gen_server:call(Pid, {send_message, Message}).

%% @private
init([UserId]) ->
    {ok, #state{user_id = UserId}}.

%% @private
handle_call({send_message, Message}, _From, State) ->
    send_message_internal(Message, State);
handle_call(_Request, _From, State) ->
    {reply, {error, unknown_call}, State}.

%% @private
handle_cast({add_channel, Channel}, State) ->
    add_channel_internal(Channel, State);
handle_cast(_Msg, State) ->
    {noreply, State}.

%% @private
handle_info({'DOWN', _Ref, process, Pid, Info}, State = #state{user_id=UserId, channels = [{Pid, _, _}]}) ->
    lager:debug("Last channel down ~p:~p. Stop session [~p]", [Pid, Info, UserId]),
    {stop, normal, State#state{channels = []}};
handle_info({'DOWN', _Ref, process, Pid, Info}, State = #state{user_id=UserId, channels = Channels}) ->
    lager:debug("Session-~p: Channel pid ~p down for reason [~p]", [UserId, Pid, Info]),
    {noreply, State#state{channels = lists:keydelete(Pid, 1, Channels)}};
handle_info(_Info, State) ->
    {noreply, State}.

%% @private
terminate(_Reason, _State) ->
    ok.

%% @private
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

add_channel_internal(Channel = {Pid, _Transport, _Socket}, StateIn = #state{channels = Channels}) ->
    lager:debug("New channel: ~p", [Channel]),
    _Ref = erlang:monitor(process, Pid),
    State = StateIn#state{channels = [Channel | Channels]},
    % {ok, StateOut} = send_deffered_messages(State),
    {noreply, State}.

send_message_internal(Message, State = #state{user_id = UserId, channels = Channels}) ->
    lager:debug("Channel - ~p:", [UserId]),
    ok = broadcast_message(Message, Channels),
    {reply, ok, State}.

broadcast_message(Message, Channels) ->
    lists:foreach(fun ({_, Transport, Socket}) ->
        lager:debug("~p - ~p", [Transport, Socket]),
        lager:debug("Sends message to [~p]", [Message#pack.addressee_id]),
        Pack = ams_pb:encode_pack(Message),
        Node = node(Socket),
        ok = rpc:call(Node, Transport, send, [Socket, Pack])
    end, Channels).
