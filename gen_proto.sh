#!/usr/bin/env bash

ROOT=`pwd`
PROTO_NAME="ams"
PROTO_FILE="${ROOT}/proto/${PROTO_NAME}.proto"

# Erlang
PROTOBUFFS_PATH=$ROOT/deps/protobuffs
cd "${PROTOBUFFS_PATH}/ebin"
$PROTOBUFFS_PATH/bin/protoc-erl $PROTO_FILE
mv "${PROTO_NAME}_pb.erl" "${ROOT}/apps/ams/src"
mv "${PROTO_NAME}_pb.hrl" "${ROOT}/apps/ams/include"

# Ruby
cd "${ROOT}/client"
bundle exec rprotoc $PROTO_FILE
