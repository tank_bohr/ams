#!/usr/bin/env ruby
require_relative './client.rb'
include Client

port = ARGV.shift || 9090
socket!(port)

user_id = 17
send_init(user_id)

addressee_id = 19
puts 'Input messages:'
while txt = gets
  txt.chomp!
  msg = Pack.new(
    :type => :MESSAGE,
    :user_id => user_id,
    :text => txt,
    :addressee_id => addressee_id,
  )
  socket.send(msg.to_s, 0)

  puts "The message [#{txt}] has been sent to #{addressee_id}"
end
