#!/usr/bin/env ruby
require_relative './client.rb'
include Client

port = ARGV.shift || 9090
socket!(port)

user_id = 19
send_init(user_id)

loop do
  puts "Waiting...\n"
  s = socket.recv(1024)
  msg = Pack.new.parse_from_string(s)
  pp(msg)
end
