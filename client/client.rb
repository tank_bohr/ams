require 'pp'
require 'socket'
require_relative './ams.pb.rb'

module Client
  attr_reader :socket

  def socket!(port=9090)
    @socket = TCPSocket.new('localhost', port)
  end

  def send_init(user_id=100)
    message = Pack.new(:type => :INIT, :user_id => user_id)
    socket.send(message.to_s, 0)
  end
end
