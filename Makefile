.PHONY: all compile deps clean distclean run nodeps

all: deps compile

compile: deps
	./rebar compile

deps:
	./rebar get-deps

clean:
	./rebar clean

distclean: clean
	./rebar delete-deps

run:
	ERL_LIBS=apps:deps erl +K true -boot start_sasl -name `uuidgen`@127.0.0.1 -config config/app -s ams

cyclops:
	ERL_LIBS=apps:deps erl +K true -boot start_sasl -noshell -name cyclops@127.0.0.1 -config config/cyclops -s ams

iceman:
	ERL_LIBS=apps:deps erl +K true -boot start_sasl -noshell -name iceman@127.0.0.1 -config config/iceman -s ams

nodeps:
	./rebar compile skip_deps=true
